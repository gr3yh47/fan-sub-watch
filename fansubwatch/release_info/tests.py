"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""


from django.core.management import setup_environ
from fansubwatch import settings
setup_environ(settings)


from django.test import TestCase
import models

class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)

        
def init_genres():
    g_list = ['Action', 'Comedy', 'Romance', 'Thriller', 'Mystery', 'Horror', 'Sci-fi', 'Fantasy']
    for g_name in g_list:
        g = models.Genre(genre = g_name)
        g.save()

def init_formats():
    f_list = ['OVA', 'Movie', 'TV']
    for f_name in f_list:
        f = models.Format(format = f_name)
        f.save()
    
    

def test_menu():    
    print '1 - DB initialization \n'
    print 'add some tests... \n'
    print 'q to quit \n'
    
    x = 0
    while (x <> 'q'):
        x = input("enter choice: ")
        
        if x == 1:
            init_genres()
            init_formats()
    
if __name__ == '__main__':
    test_menu()
    
    
    