from calendar import HTMLCalendar
from datetime import date
from itertools import groupby

from django.utils.html import conditional_escape as esc

class ReleaseCalendar(HTMLCalendar):

    def __init__(self, animes):
        super(ReleaseCalendar, self).__init__()
        self.animes = self.group_by_day(animes)

    def formatday(self, day, weekday):
        if day != 0:
            cssclass = self.cssclasses[weekday]
            if date.today() == date(self.year, self.month, day):
                cssclass += ' today'
            if day in self.animes:
                cssclass += ' filled'
                body = ['<ul>']
                for anime in self.animes[day]:
                    body.append('<li>')
                    body.append('<a href="%s">' % anime.get_absolute_url())
                    body.append(esc(anime.title))
                    body.append('</a></li>')
                body.append('</ul>')
                return self.day_cell(cssclass, '%d %s' % (day, ''.join(body)))
            return self.day_cell(cssclass, day)
        return self.day_cell('noday', '&nbsp;')

    def formatmonth(self, year, month):
        self.year, self.month = year, month
        return super(ReleaseCalendar, self).formatmonth(year, month)

    def group_by_day(self, anime):
        field = lambda anime: anime.release_date
        return dict(
            [(day, list(items)) for day, items in groupby(anime, field)]
        )

    def day_cell(self, cssclass, body):
        return '<td class="%s">%s</td>' % (cssclass, body)