from django.db import models
from datetime import date
    
    
class Genre(models.Model):
    genre = models.CharField(max_length=20)
    
    def __unicode__(self):
        return self.genre
    
class FansubGroup(models.Model):
    name = models.CharField(max_length=40)
    website = models.URLField(blank=True)
    
    def __unicode__(self):
        return self.name
    
    
class Format(models.Model):
    format = models.CharField(max_length=10) #OVA, Movie, TV
    
    def __unicode__(self):
        return self.format
    

class Anime(models.Model):
    title = models.CharField(max_length=50)
    alt_title = models.CharField(max_length=50, blank=True)
    abbreviation = models.CharField(max_length=15, blank=True)
    release_date = models.DateField(blank=True, null=True)
    synopsis = models.TextField(blank=True)
    #primary_genre = models.ForeignKey(Genre)
    genre = models.ManyToManyField(Genre, blank=True, null=True)
    fansub_group = models.ForeignKey(FansubGroup, blank=True, null=True)
    info_url = models.URLField(blank=True)
    format = models.ManyToManyField(Format)
    tv_seasons = models.PositiveIntegerField(blank=True, null=True)
    tv_episodes = models.PositiveIntegerField(blank=True, null=True)
    movies = models.PositiveIntegerField(blank=True, null=True)
    ovas = models.PositiveIntegerField(blank=True, null=True)
    
    def __unicode__(self):
        return self.title
        
    def upcoming_releases(self):
        #first_of_month = date.today().replace(day=1)
        return self.episode_set.filter(release_date__gte=date.today())
        

class Episode(models.Model):
    episode_num = models.PositiveIntegerField()
    season_num = models.PositiveIntegerField(blank=True, null=True)
    season_episode_num = models.PositiveIntegerField(blank=True, null=True)
    anime = models.ForeignKey(Anime)
    release_date = models.DateField()
    
    def __unicode__(self):
        return "%s-%s" % (self.anime.abbreviation or self.anime, self.episode_num)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    