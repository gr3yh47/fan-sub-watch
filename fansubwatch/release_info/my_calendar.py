import calendar
from datetime import date, timedelta
from fansubwatch.release_info.models import Anime, Episode

class ReleaseCalendar(object):
    
    def __init__(self, year=None, month=None):
        
        todays_date = date.today()
        
        if not year:
            year = todays_date.year
            
        if not month:
            month = todays_date.month
        
        
        self.date = date(int(year), int(month), 1)
        self.month_string = self.date.strftime('%B')
        
        first_day, self.total_days = calendar.monthrange(int(year), int(month))
        first_day = (first_day + 1) % 7
        
        self.header = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        
        
        #populate a list with lists representing calendar rows (weeks) for display
        self.rows_list = []
        remaining_days = (self.total_days - (7-first_day))
        num_rows = remaining_days / 7 + 1
        if remaining_days % 7:
            num_rows += 1
        for i in xrange(num_rows): #+2 for first and last week iterations, trust me it works
            
            one_week = []
            for j in xrange(7 - len(one_week)):
                day_num = j + (7 * i + 1) - first_day
                try:
                    this_date = self.date.replace(day=day_num)
                    items = Episode.objects.filter(release_date=this_date)
                except ValueError:
                    day_num = ''
                    items = []
                one_week.append((str(day_num), items))
                    
            self.rows_list.append(one_week)
            
    
        self.prev_month = (self.date.replace(day=1) - timedelta(days=1)).month
        self.prev_year = (self.date.replace(day=1) - timedelta(days=1)).year
   
        self.next_month = (self.date.replace(day=self.total_days) + timedelta(days=1)).month
        self.next_year = (self.date.replace(day=self.total_days) + timedelta(days=1)).year
        
        
        
        
        