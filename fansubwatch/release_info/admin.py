from django.db import models
from django.contrib import admin
from django.forms import CheckboxSelectMultiple
from fansubwatch.release_info.models import Anime, FansubGroup, Genre, Format, Episode


class AnimeAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }
    #list_display = ('title', 'primary_genre')
    search_fields = ('title',)
    #list_filter = ('primary_genre')
    ordering = ('title',)
    filter_horizontal = ('genre',)

admin.site.register(Anime, AnimeAdmin)
admin.site.register(FansubGroup)
admin.site.register(Genre)
admin.site.register(Episode)
admin.site.register(Format)