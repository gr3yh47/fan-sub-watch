# Create your views here.
from datetime import date
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.utils.safestring import mark_safe
from fansubwatch.release_info.models import Anime
from fansubwatch.release_info.my_calendar import ReleaseCalendar

def landing(request):
    return render(request, 'release_info/landing.html')
    
    
# def test_calendar(request, year=datetime.date.today().year, month=datetime.date.today().month):
  # my_animes = Anime.objects.order_by('release_date').filter(
    # release_date__year=year, release_date__month=month
  # )
  # cal = ReleaseCalendar(my_animes).formatmonth(year, month)
  # return render_to_response('release_info/testcal.html', {'calendar': mark_safe(cal),})
  
  
def anime_detail(request, db_id):
    
    try:
        anime = Anime.objects.get(id=int(db_id))
        return render(request, 'release_info/anime_detail.html', {'anime': anime})
    except Anime.DoesNotExist:
        return render(request, 'error.html', {'error': 'Anime not found!'})

       
def release_calendar(request, year=None, month=None):
    
    calendar = ReleaseCalendar(year, month)
   
    return render(request, 'release_info/testcal.html', {'calendar': calendar})
    
    
    
    