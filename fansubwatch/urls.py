from django.conf.urls import patterns, include, url
import release_info.views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^release_info/$', release_info.views.landing),
    url(r'^release_info/calendar/$', release_info.views.release_calendar),
    url(r'^release_info/calendar/(?P<month>\d{1,2})/$', release_info.views.release_calendar),
    url(r'^release_info/calendar/(?P<year>\d{4})/(?P<month>\d{1,2})/$', release_info.views.release_calendar),    
    url(r'^release_info/anime_detail/(\d{1,5})/$', release_info.views.anime_detail),
    # Examples:
    # url(r'^$', 'fansubwatch.views.home', name='home'),
    # url(r'^fansubwatch/', include('fansubwatch.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
