# README #

### Anime Fansub Release Calendar ###

* Anime released in japan often never makes it to the states.
* Fansub groups translate and subtitle such anime.
* This is a very spartan prototype app of a concept for a website to track upcoming releases

### How do I get set up? ###

* Install django and python 2.7
* Uses sqlite so no db setup is necessary
* python manage.py runserver to start built in dev server
